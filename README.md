# Demo Works

1. [Discover and display anagrams from string and text file.](https://bitbucket.org/vivyad/anagrams)
2. [Custom implementation of the Tree data-structure](https://bitbucket.org/vivyad/treealgos)
3. [SpringBoot Rest Implementation with MariaDb for Customer data-model](https://bitbucket.org/vivyad/springbootmariadb/)